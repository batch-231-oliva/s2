/*alert("alo")*/

// without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// spaghetti code - code is not organized enough that it becomes hard to work with
// encapsulation - organazie related information (properties) and behavior (methods) to belong to a single entity


//create student one
/*let studentOneName = 'Tony';
let studentOneEmail = 'starksindustries@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Peter';
let studentTwoEmail = 'spideyman@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Wanda';
let studentThreeEmail = 'scarlettMaximoff@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Steve';
let studentFourEmail = 'captainRogers@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}*/


// Encapsulate the following information into 4 student objects using object literals

let studentOne = {
	name: "Tony",
	email: "starkindustries@mail.com",
	grades: [89,84, 78, 88],

	// add the functionalites available to a student as object methods
		// kewords "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
    console.log(`${email} has logged out`);
	},

	listGrades(grades){
    	this.grades.forEach(grade => {
        	console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
	},

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade)
		return sum / 4;
	},

	//mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise
	// name the method willPass

	willPass(){
		if (this.computeAve() >= 85) {
			return true
		} else {
			return false
		}

		/* return this.computeAve() >= 85 ? true : false
			syntax: condition ? value if condition is true : value if condition is false
		*/
	},

	willPassWithHonors(){
		return (this.willPass()  && this.computeAve() >= 90) ? true : false
	}

}


console.log(`Student one's name is ${studentOne.name}`)
console.log(studentOne.computeAve())
console.log(studentOne.willPass())
console.log(studentOne.willPassWithHonors())
console.log(this) // result: global window object

let studentTwo = {
	name: "Peter",
	email: "spideyman@mail.com",
	grades: [78, 82, 79, 85],

	// add the functionalites available to a student as object methods
		// kewords "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
    console.log(`${email} has logged out`);
	},

	listGrades(grades){
    	this.grades.forEach(grade => {
        	console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
	},

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade)
		return sum / 4;
	},

	//mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise
	// name the method willPass

	willPass(){
		if (this.computeAve() >= 85) {
			return true
		} else {
			return false
		}

		/* return this.computeAve() >= 85 ? true : false
			syntax: condition ? value if condition is true : value if condition is false
		*/
	},

	willPassWithHonors(){
		return (this.willPass()  && this.computeAve() >= 90) ? true : false
	}

}

let studentThree = {
	name: "Wanda",
	email: "scarlettMaximoff@mail.com",
	grades: [87, 89, 91, 93],

	// add the functionalites available to a student as object methods
		// kewords "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
    console.log(`${email} has logged out`);
	},

	listGrades(grades){
    	this.grades.forEach(grade => {
        	console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
	},

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade)
		return sum / 4;
	},

	//mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise
	// name the method willPass

	willPass(){
		if (this.computeAve() >= 85) {
			return true
		} else {
			return false
		}

		/* return this.computeAve() >= 85 ? true : false
			syntax: condition ? value if condition is true : value if condition is false
		*/
	},

	willPassWithHonors(){
		return (this.willPass()  && this.computeAve() >= 90) ? true : false
	}

}

let studentFour = {
	name: "Steve",
	email: "captainRogers@mail.com",
	grades: [91, 89, 92, 93],

	// add the functionalites available to a student as object methods
		// kewords "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
    console.log(`${email} has logged out`);
	},

	listGrades(grades){
    	this.grades.forEach(grade => {
        	console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
	},

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade)
		return sum / 4;
	},

	//mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise
	// name the method willPass

	willPass(){
		if (this.computeAve() >= 85) {
			return true
		} else {
			return false
		}

		/* return this.computeAve() >= 85 ? true : false
			syntax: condition ? value if condition is true : value if condition is false
		*/
	},

	willPassWithHonors(){
		return (this.willPass()  && this.computeAve() >= 90) ? true : false
	}

}

// Mini-Quiz
/*
1.) Spaghetti Code
2.) {key:value_pairs},
3.) encapsulation
4.) student1.enroll()
5.) true
6.) global window object
7.) true
8.) true
9.) true
10.) true
*/

const classOf1A = {
	students : [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				result++;
			}
		})
		return result;
		// result: 2
	},

	// Function Coding Activity:

	honorsPercentage(){
		return ((100 / this.students.length) * this.countHonorStudents())
	},

	retrieveHonorStudentInfo(){
		let studentsWtihHonor = []
		this.students.map(student => {
			if (student.willPassWithHonors()){
				studentsWtihHonor.push({name: student.email, aveGrade: student.computeAve()})
			}
		})
		return studentsWtihHonor
	},

	sortHonorStudentsByGradeDesc(){
		let toBeSort = this.retrieveHonorStudentInfo()
		return (toBeSort.sort((a, b) => b.aveGrade - a.aveGrade));
	}
	
}

console.log(classOf1A.countHonorStudents())
console.log(classOf1A.honorsPercentage())
console.log(classOf1A.retrieveHonorStudentInfo())
console.log(classOf1A.sortHonorStudentsByGradeDesc())